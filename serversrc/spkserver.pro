#QT += core
QT -= gui

TARGET = spkserver
CONFIG += console
CONFIG -= app_bundle
QMAKE_CXXFLAGS +=  -std=c++11 -Wno-unused-paramater
TEMPLATE = app
INCLUDEPATH = ./../common ./

SOURCES += main.cpp \
    ../common/alsacls.cpp \
    ../common/aocls.cpp \
    ../common/aoutil.cpp \
    ../common/clictx.cpp \
    ../common/clithread.cpp \
    ../common/cpipein.cpp \
    ../common/mp3cls.cpp \
    ../common/sock.cpp

HEADERS += \
    ../common/alsacls.h \
    ../common/aocls.h \
    ../common/aoutil.h \
    ../common/clictx.h \
    ../common/clithread.h \
    ../common/cpipein.h \
    ../common/main.h \
    ../common/mp3cls.h \
    ../common/os.h \
    ../common/sock.h

#sudo apt-get install libmpg123-dev libao-dev



win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../../usr/lib/x86_64-linux-gnu/release/ -lao
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../../../usr/lib/x86_64-linux-gnu/debug/ -lao
else:unix: LIBS += -L$$PWD/../../../../../usr/lib/x86_64-linux-gnu/ -lao





INCLUDEPATH += $$PWD/../../../../../usr/lib/x86_64-linux-gnu
DEPENDPATH += $$PWD/../../../../../usr/lib/x86_64-linux-gnu

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../usr/lib/x86_64-linux-gnu/release/libao.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../usr/lib/x86_64-linux-gnu/debug/libao.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../usr/lib/x86_64-linux-gnu/release/ao.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../usr/lib/x86_64-linux-gnu/debug/ao.lib
else:unix: PRE_TARGETDEPS += $$PWD/../../../../../usr/lib/x86_64-linux-gnu/libao.a

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../../usr/lib/x86_64-linux-gnu/release/ -lmpg123
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../../../usr/lib/x86_64-linux-gnu/debug/ -lmpg123
else:unix: LIBS += -L$$PWD/../../../../../usr/lib/x86_64-linux-gnu/ -lmpg123

INCLUDEPATH += $$PWD/../../../../../usr/lib/x86_64-linux-gnu
DEPENDPATH += $$PWD/../../../../../usr/lib/x86_64-linux-gnu

LIBS +=  -lasound
