
#include <set>
#include "main.h"
#include "cpipein.h"
#include "clithread.h"
#include "aoutil.h"
#include "mp3cls.h"
#include "alsacls.h"
#include "clictx.h"

bool    __alive = true;
RunCtx* PCTX;


void block_signal(int signal_to_block /* i.e. SIGPIPE */ )
{
    sigset_t set;
    sigemptyset(&set);
    sigaddset(&set, signal_to_block);
    sigset_t old_state;
    sigprocmask(SIG_BLOCK, &set, &old_state);
}

void notapl(int s)
{
    (void)s;
}

void ctrlc(int s)
{
    (void)s;
    __alive=false;
}


int main(int argc, char *argv[])
{
    RunCtx  ctx(&PCTX);

    (void)argc;
    (void)argv;
    ctx.server();
}

RunCtx::RunCtx(RunCtx** p)
{
    *p=this;
    //block_signal(SIGPIPE);
    signal(SIGINT, ctrlc);
    signal(SIGPIPE, notapl);
}

RunCtx::~RunCtx()
{

}

void RunCtx::server()
{
    CPipeIn         pipe(PIPE_IN);
    TcpSrv          srv;
    udp_group_sock  mcast;
    Mp3Cls          mp3;

    system("clear");
    pc(0,0, "spkserver ver: %s",_VERSION);
    mcast.create(MC_ADDR, MC_PORT);
    if(pipe.ok() && srv.listen())
    {
        Qbuff::Chunk    chunk(UDP_CHUNK );
        int             frm=0;
        uint32_t        thisip    = (uint32_t)inet_addr(sock::GetLocalIP("127.0.0.1"));
        SADDR_46        test(thisip,0);
        bool            activity  = false;
        size_t          sleept    = (1000*UDP_CHUNK) / (SAMPLE_AO * CHANNELS_AO * (BITS_AO/8));
        int             isleptMax = sleept<<1;
        int             draining=0;
        size_t          ct;

        pc(0,2, "Listening on: %s",test.c_str());

        while(__alive)
        {
            ct = GetTickCount();
            activity = srv.pool();
            chunk._hdr->bytes = pipe.peek(chunk.payload(), chunk.len());
            chunk._hdr->frmsig=UNIQUE_HDR;
            chunk._hdr->saddr=thisip;
            if(chunk._hdr->bytes==0)
            {
                if(++draining>100)
                {
                    chunk._hdr->sig = RESET_ALL;
                    frm = 0;
                }
                chunk._hdr->sig = EMPTY_DATA;
                chunk._hdr->seq = 0;
                mcast.send((uint8_t*)chunk.buff(), chunk.size(), MC_PORT, MC_ADDR);
                MSLEEP(isleptMax);
                continue;
            }
            draining=0;
            chunk._hdr->seq=frm++;
            chunk._hdr->saddr=thisip;
            chunk._hdr->sig=PLAY_DATA;
            chunk._hdr->size=chunk._sz;
            chunk._hdr->frmmax=srv.maxfrm();
            chunk._hdr->pingmax=srv.maxping();
            if(!activity)
            {
                chunk._hdr->sig=ALIVE_DATA;
            }
            mcast.send((uint8_t*)chunk.buff(), chunk.size(), MC_PORT, MC_ADDR);
            pc(0,6,"Sent:  %d bytes,  Seq: %d    Sleep: %d ",chunk._hdr->bytes, chunk._hdr->seq, sleept);

            if(activity){
                MSLEEP(int(sleept * 0.8));
            }
            else
                MSLEEP(sleept);
        }
    }
    mcast.destroy();
}

void RunCtx::client()
{
}
