#ifndef MP3CLS_H
#define MP3CLS_H

#include <stdint.h>
#include <mpg123.h>

class Mp3Cls
{
public:
    Mp3Cls();
    ~Mp3Cls();


    bool open(const char* file);
    bool close();
    ulong getframe()const;
    void setframe(ulong frmsig);
    size_t read(uint8_t* buff, size_t maxlen);

    size_t openFeed(const uint8_t* in, size_t len );
    bool feed(const uint8_t* in, size_t len );

public:
    int             _frmlen;
    int             _bits;
    long int        _rate;
    int             _channels;

private:
    mpg123_handle*  _mh;
    int             _err;

};

#endif // MP3CLS_H
