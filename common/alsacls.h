#ifndef ALSACLS_H
#define ALSACLS_H

#include <stdio.h>
#include <inttypes.h>
#include <alsa/asoundlib.h>
#include <stdio.h>

#define PCM_DEVICE "default"

class Alsacls
{
public:
    Alsacls();
    ~Alsacls();

    int open(int rate, int channels, int seconds);
    void close();
    void playbuffer(uint8_t * pb, int len);
private:

    unsigned int _rate;
    int _channels;
    int _seconds;
    int _pcm;
    snd_pcm_t *_pcm_handle;
    snd_pcm_hw_params_t *_params;
    snd_pcm_uframes_t _frames;
    uint8_t           *_buff;

};

#endif // ALSACLS_H
