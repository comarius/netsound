﻿
#include <unistd.h>
#include <set>
#include <vector>
#include <sstream>
#include <fcntl.h>
#include "sock.h"
#include "main.h"
#include "clictx.h"

static size_t split(const std::string& s, char delim, std::vector<std::string>& a);

TcpSrv::TcpSrv()
{
    _curt = GetTickCount()+3000;
    _maxping = 0;
    _cli = 0;
    _s=0;
    _dirty=false;
    _maxping=0;
    _slowerip=0;
    _maxseq=0;
    _curt = 0;
    _newclient=false;
}

TcpSrv::~TcpSrv()
{
    destroy();
}

bool TcpSrv::listen()
{
    int ntry = 0;
AGAIN:
    if(__alive==false)
        return false;
    if(this->create(SRV_PORT+1, SO_REUSEADDR, 0)>0)
    {
        fcntl(this->socket(), F_SETFD, FD_CLOEXEC);
        if(this->tcp_srv_sock::listen(8)!=0)
        {
            std::cout <<"socket can't listen. Trying "<< (ntry+1) << " out of 10 " << std::endl;

            this->tcp_srv_sock::destroy();
            sleep(1);
            if(++ntry<10)
                goto AGAIN;
            return false;
        }
        std::cout << "listening \n";
        return true;
    }
    return false;
}

bool TcpSrv::pool()
{
    bool    req=false;
    fd_set  rd;
    int     ndfs = this->socket();// _s.sock()+1;
    int     sleept = (1000*UDP_CHUNK) / (SAMPLE_AO * CHANNELS_AO * (BITS_AO/8));
    timeval tv {0, sleept * 4000};
    uint8_t  loco[MAX_SHARED];

    FD_ZERO(&rd);
    FD_SET(this->socket(), &rd);
    for(auto& s : _clis)
    {
        if(s->socket()>0)
        {
            FD_SET(s->socket(), &rd);
            ndfs = std::max(ndfs, s->socket());
        }
        else
            _dirty = true;
    }
    int is = ::select(ndfs+1, &rd, 0, 0, &tv);
    if(is ==-1) {
        std::cout << "socket select() " << std::endl;
        __alive=false;
        return req;
    }
    if(is)
    {
        if(FD_ISSET(this->socket(), &rd))
        {
            Clictx* cs = new Clictx(this);
            if(this->accept(*cs)>0)
            {
                //std::cout << "new connection \n";
                cs->_index = _clis.size();
                pc(0,10+cs->_index, "%04d: New connection from: %s", cs->Rsin().c_str());
                cs->setDirty();
                cs->set_blocking(1);

                _clis.push_back(cs);

                _curt = GetTickCount()+3000;
                _maxping=0;
                _slowerip=0;
                _maxseq=0;
                _curt=0;
                _newclient=true;
            }
            else
            {
                delete cs;
            }
        }

        for(auto& s : _clis)
        {
            if(s->socket()<=0){
                _dirty=true;
                continue;
            }
            if(FD_ISSET(s->socket(), &rd))
            {
                int rt = s->treceive(loco, MAX_SHARED-1);
                if(rt==0) //con closed
                {
                    pc(0,10+s->_index, "%04d: Connection closed: %s");
                    s->destroy();
                    _dirty = true;
                }
                else if(rt > 0)
                {
                    req |= s->keeptrack(_maxping,_maxseq);
                }
            }
        }
    }
    if(_dirty)
        _clean();

    for(auto& s : _clis)
    {
        s->replytrack(_newclient);
    }
    _newclient=false;
    uint64_t curt = GetTickCount();
    if(curt - _curt > 3000 && _maxping>0 )
    {
        BufHdr  h;

        h.frmsig   = UNIQUE_HDR;
        h.bytes    = 0;
        h.sig      = SYNC_ALL;
        h.pingmax  = _maxping;
        h.frmmax  = _maxseq;

        for(auto& s : _clis)
        {
            s->send((uint8_t*)&h,sizeof(h));
        }
        pc(0,3,"Synching All Max ping:  %d           ",_maxping);
        _maxping = 0;
        _curt = curt;
    }
    return req;
}

void TcpSrv::_clean()
{
AGAIN:
    for(std::vector<Clictx*>::iterator s=_clis.begin();s!=_clis.end();++s)
    {
        if((*s)->socket()<=0)
        {
            delete (*s);
            _clis.erase(s);
            pc(0,10+(*s)->_index, "%04d: Client %s gone             ", (*s)->Rsin().c_str());
            _maxping=0;
            _slowerip=0;
            _maxseq=0;
            goto AGAIN;
        }
    }
    _dirty=false;
}


void TcpSrv::setVolume(uint32_t htip, int vol)
{
    BufHdr h;
    h.a_bits=0;
    h.frmsig = UNIQUE_HDR;
    h.sig    = VOLUM_SIG;
    h.bytes    = vol;
    for(auto& s : _clis)
    {
        if(s->Rsin().ip4() == htip)
        {
            s->send((uint8_t*)&h, sizeof(h));
            s->_volume = vol;
        }
    }
}

int  TcpSrv::getVolume(uint32_t htip)const
{
    for(auto& s : _clis)
    {
        if(s->Rsin().ip4() == htip)
        {
            return s->_volume;
        }
    }
    return 0;
}


bool TcpSrv::destroy(bool be)
{
    tcp_srv_sock::destroy(be);
    for(auto& s : _clis)
    {
        delete s;
    }
    return true;
}


Clictx::Clictx(TcpSrv*  psrv):_psrv(psrv)
{
    _dirty = false;
    _stats = false;
    _remtick = 0;
    _player = 0;
    _volume = 30;
    _index = 0;

}

Clictx::~Clictx()
{

}

bool Clictx::keeptrack(uint32_t& maxping, uint32_t& frmmax)
{
    if(this->_req._hdr.sig == PLAY_DATA ||
       this->_req._hdr.sig == SYNC_DATA )
    {
        if(_req._hdr.pingt > maxping)
        {
            maxping = _req._hdr.pingt;
        }
        if(_req._hdr.frmmax > frmmax)
        {
            frmmax = _req._hdr.frmmax;
        }
        if(SYNC_DATA == this->_req._hdr.sig)
        {
            _req._hdr.sig = SYNC_DATA;
            _req._hdr.frmmax = frmmax;
            _stats = true;
        }
    }
    return this->_req._hdr.sig == PLAY_DATA ;
}

bool Clictx::replytrack(bool reset)
{
    if(reset==false && _stats==false)
        return false;

    if(reset)
        _req._hdr.sig = RESET_ALL;
    else
        _req._hdr.sig = SYNC_DATA;
    _stats = false;

    pc(0,10+_index, "%04d:  Ping Reply with: %05d           ",_index, int(_req._hdr.seq));

    if(this->send((const uint8_t*)&this->_req, sizeof(BufHdr))<=0)
    {
        std::cout << "tcp src gone \n";
        this->destroy();
        _dirty=true;
    }
    ::memset(&_req,0,sizeof(_req));
    return true;
}

/**
 * @brief Clictx::receive, hope to exhaust this, and read all
 * @param pbuff
 * @param sz
 * @return
 */
int   Clictx::treceive(uint8_t* pbuff, int sz)
{
    _player=false;
    ::memset(&_req,0,sizeof(_req));
    int bytes = this->receive((unsigned char*)pbuff, sz);
    if(bytes >= int(sizeof(BufHdr)))
    {
        BufHdr* ph = (BufHdr*)pbuff;
        if(ph->frmsig == UNIQUE_HDR)
        {
            //std::cout <<"seq" << bytes << " seq" << int(ph->sig) <<"\n";
            ::memcpy(&_req,pbuff,sizeof(_req));
            _player=true;
        }
        else
        {
            char* pc = (char*)pbuff;
            if(!strncmp(pc,"GET ",4))
            {
                pc[bytes]=0;
                _htmlReply(pc);
            }
            this->destroy();
            return 0;
        }
        return bytes;
    }
    return 0;
}

void Clictx::_htmlReply(char* req)
{
    std::vector<std::string> preds;
    std::string reply = "";
    char* pt = ::strtok(req," ");
    pt = ::strtok(0," ");
    if(pt)
    {
        std::string req = pt;
        std::string resp;
        split(req,'/',preds);

        if(preds[0]=="speakers")
        {
            const std::vector<Clictx*>& cs = _psrv->clis();
            for(const auto& c : cs)
            {
                if(c->_player==false)
                    continue;
                resp+=std::to_string(c->Rsin().ip4());
                resp+=",";
            }
        }
        if(preds[0]=="speaker" && preds.size()>1)
        {
            std::string ip = preds[1];
            if(preds.size()>2)
            {
                _psrv->setVolume(::atol(preds[1].c_str()),
                                 ::atol(preds[2].c_str()));
            }
            else //get
            {
                resp+=_psrv->getVolume(std::stoi(preds[1]));
            }
        }
        std::string htreply  = "HTTP/1.1 200 OK\r\n";
        htreply += "Cache-Control : no-cache, private\r\n";
        htreply += "Content-Length :" + std::to_string(resp.length());
        htreply +=  "\r\n";
        htreply += "Content-Type: application/text\r\n\r\n";
        htreply += resp;
        this->send(htreply.c_str(), htreply.length());
    }
}


static size_t split(const std::string& s, char delim, std::vector<std::string>& a)
{
    std::string st;
    std::istringstream ss(s);
    a.clear();
    while (getline(ss, st, delim))
    {
        if(!st.empty())
            a.push_back(st);
    }
    return a.size();
}

