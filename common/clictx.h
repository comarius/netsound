#ifndef CLICTX_H
#define CLICTX_H

#include <vector>
#include "sock.h"
#include "clithread.h"

#define MAX_SHARED (sizeof(BufHdr)<<8)
class TcpSrv;
class Clictx : public tcp_cli_sock
{
public:
    Clictx(TcpSrv*  psrv);
    virtual ~Clictx();
    int     treceive(uint8_t* s, int sz);
    bool    keeptrack(uint32_t& s,uint32_t& q);
    bool    replytrack(bool reset);
    void    setDirty(){_dirty=true;};
    bool    isDirty()const{return _dirty;}
private:
    void    _htmlReply(char* pc);
    union   U{
        char    _req[sizeof(BufHdr)];
        BufHdr  _hdr;
    }           _req;
    bool        _dirty;
public:
    bool        _stats;
    uint32_t    _remtick;
    TcpSrv*     _psrv;
    bool        _player;
    int         _volume;
    int         _index;
};

class TcpSrv : public tcp_srv_sock
{
public:
    TcpSrv();
    virtual ~TcpSrv();
    bool    destroy(bool be=true);
    bool    listen();
    bool    pool();
    uint32_t maxping()const{return _maxping;};
    uint32_t maxfrm()const{return _maxseq;};
    const std::vector<Clictx*>& clis()const{return _clis;};
    void setVolume(uint32_t htip, int vol);
    int  getVolume(uint32_t htip)const;

private:
    void    _clean();

private:
    std::vector<Clictx*> _clis;
    int                  _cli;
    int                  _s;
    bool                 _dirty;
    uint32_t             _maxping;
    uint32_t             _slowerip;
    uint32_t             _maxseq;
    uint64_t             _curt;
    bool                 _newclient;
};



#endif // CLICTX_H
