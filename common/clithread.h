#ifndef RECTHraccess_H
#define RECTHraccess_H

#include <iostream>
#include <thread>
#include <mutex>
#include <vector>
#include <deque>
#include "os.h"

#define SRV_PORT        18722
#define CLI_PORT        18720
#define MC_ADDR         "238.8.255.255"
#define UNIQUE_HDR       0xFFFF7777FFFF7777
#define MC_PORT         10007

#define VOLUM_SIG       10
#define SYNC_DATA       11
#define FRM_SIG         12
#define SETT_SIG        13
#define RESTA_SIG       14
#define SYNC_ALL        15
#define ALIVE_DATA      16
#define RESET_ALL       17
#define PLAY_DATA       18
#define EMPTY_DATA      19


#define UDP_CHUNK       8912
#define CHUNKS          64
#define OPTIM_QUEUE_LEN 80
#define ALMOST_FULL     95

typedef struct
{
    uint64_t frmsig;
    uint8_t  sig;       //allways FD
    uint32_t seq;
    uint32_t tickc;
    uint32_t pingt;
    uint32_t pingmax;
    uint32_t frmmax;
    uint32_t saddr;
    bool     locked;
    int      bytes;
    int      size;
    int      a_rate;
    int      a_byte;
    int      a_channels;
    int      a_bits;
}  __attribute__ ((packed)) BufHdr ;


class Qbuff
{
public:

    struct Chunk
    {
        explicit Chunk(int sz):_sz(sz + sizeof(BufHdr)){

            _buff = new uint8_t [sz + sizeof(BufHdr)];
            ::memset(_buff,0,sz);
            _hdr = (BufHdr*)_buff;

        }
        ~Chunk(){
            delete[] _buff;
        }
        uint8_t*  payload()const{return _buff+sizeof(BufHdr);}
        uint8_t*  buff()const{return _buff;}
        int       size()const{return _sz;}
        int       len()const{return _sz-sizeof(BufHdr);}
        BufHdr*   _hdr=0;
        uint8_t*  _buff=0;
        int       _sz;
    };

    Qbuff(int chunks, int chunk):_maxqsz(chunks){
        _full=ALMOST_FULL;
        _optim=OPTIM_QUEUE_LEN;
        _chunksz = (sizeof(BufHdr)+chunk);
        for(int i=0;i<chunks;i++)
        {
            Chunk* k = new  Chunk(_chunksz);
            _pool.push_back(k);
        }

    }
    ~Qbuff()
    {
        reset();
        for(auto& a : _pool){
           delete a;
        }
    }
    void resize(size_t max)
    {
        if(max < _maxqsz )
        {
            removeback((_maxqsz-max)+1);
            _maxqsz=max;
        }
    }

    uint32_t maxsz()const{return _maxqsz;}
    int payload()const{ return _chunksz;}
    int percent()const{ return (_queue.size()*100) / _maxqsz; }
    int almostFull()const{return _full;}
    int optim()const{return _optim;}
    void setFull(int f){_full=f;}
    void setOptim(int f){_optim=f;}

    void removeback(int els)
    {
        Chunk*  k;
        std::lock_guard<std::mutex> guard(_m);
        while(--els>0 && _queue.size())
        {
            k = _queue.front();
            _queue.pop_front();
            k->_hdr->locked=false;
            k->_hdr->bytes = 0;
            _pool.push_back(k);
        }
    }

    void reset()
    {
        std::lock_guard<std::mutex> guard(_m);
        while(_queue.size())
        {
            Chunk* k = _queue.front();_queue.pop_front();
            k->_hdr->locked=false;
            _pool.push_back(k);
        }
    }
    size_t length(){
        std::lock_guard<std::mutex> guard(_m);
        return _queue.size();
    }

    Chunk*  obtain()
    {
        if(_pool.size())
        {
            std::lock_guard<std::mutex> guard(_m);
            Chunk* p = _pool.back();
            p->_hdr->locked=true;
            _pool.pop_back();
            return p;
        }
        return 0;
    }

    void addend(Chunk* p)
    {
        std::lock_guard<std::mutex> guard(_m);
        _queue.push_back(p);
    }

    Chunk* getfront()
    {
        if(_queue.size())
        {
            std::lock_guard<std::mutex> guard(_m);
            Chunk* pk = _queue.front();
            _queue.pop_front();
            return pk;
        }
        return 0;
    }

    void release(Chunk* p)
    {
        p->_hdr->locked=false;
        _pool.push_back(p);
    }

private:
    std::vector<Chunk*>  _pool;
    std::deque<Chunk*>   _queue;
    size_t               _chunksz;
    size_t               _maxqsz;
    std::mutex           _m;
    int                  _full;
    int                  _optim;
};


class CliThread : public OsThread
{
public:
    CliThread(Qbuff* qbuff);//Qbuff* qbuff
    virtual ~CliThread();//Qbuff* qbuff
    void thread_main();

private:
    bool _connecting(udp_group_sock& m,
                     tcp_cli_sock& tcp);
    void _apply(BufHdr* ph);
    bool _reconnect(tcp_cli_sock& tcp, udp_group_sock& m);
    bool _netReceive(tcp_cli_sock& tcp, udp_group_sock& m, int sel);
    void _netSend(tcp_cli_sock& tcp, int& onceinawhile);

private:
    uint8_t *_drainbuff;
    Qbuff*   _pq;
    uint32_t _pingt;
    uint32_t _cliticku;
    uint32_t _diff;
    uint32_t _frmmax;
    BufHdr   _hdr;
    uint32_t _frmtime;
public:
    int      _index;
};

#endif // RECTHraccess_H
