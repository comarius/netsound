
#include "main.h"
#include "aoutil.h"
#include "sock.h"
#include <assert.h>
#include "clithread.h"
#include "clithread.h"


#define PRE_WORK_TO 256
#define SELECT_TIME 200000
#define TCP_SET     0x1
#define UDP_SET     0x2

CliThread::CliThread(Qbuff* qbuff)
{
    _pq = qbuff;
    _pq = 0;
    _pingt = 0;
    _cliticku = 0;
    _diff     = 0;
    _frmmax   = 0;
    _drainbuff = new uint8_t[UDP_CHUNK+sizeof(BufHdr)];
}


CliThread::~CliThread()
{
    delete[] _drainbuff;
}

bool CliThread::_reconnect(tcp_cli_sock& tcp, udp_group_sock& m)
{
    if(!tcp.is_really_connected())
    {
        _pingt    = 0;
        _cliticku = 0;
        _diff     = 0;
        _frmmax   = 0;
        if(!_connecting(m, tcp))
        {
            MSLEEP(PRE_WORK_TO);
            return false;
        }
    }
    return true;
}

bool CliThread::_netReceive(tcp_cli_sock& tcp, udp_group_sock& m, int sel)
{
    timeval        tv;
    fd_set          rd;

    FD_ZERO(&rd);
    FD_SET(tcp.socket(), &rd);
    FD_SET(m.socket(), &rd);
    int tmpv = std::max(m.socket(), tcp.socket())+1;
    tv.tv_sec = 0; tv.tv_usec=(sel);
    if((tmpv=::select(tmpv, &rd, 0, 0, &tv))<0)
    {
        perror("select");
        __alive=false;
        return false;
    }
    if(FD_ISSET(tcp.socket(), &rd))
    {
        tmpv = tcp.receive((uint8_t*)_drainbuff,UDP_CHUNK+sizeof(BufHdr));
        if(tmpv>=int(sizeof(BufHdr)))
        {
            _apply((BufHdr*)_drainbuff);
        }
        FD_CLR(tcp.socket(), &rd);
    }
    if(FD_ISSET(m.socket(), &rd))
    {
        Qbuff::Chunk*  chunk = _pq->obtain();
        if(chunk != 0)
        {
            int bytes = m.receive((uint8_t*)chunk->buff(),_pq->payload(),MC_PORT,MC_ADDR);
            if(bytes>0)
            {
                if(chunk->_hdr->sig==PLAY_DATA)
                {
                    _pq->addend(chunk);
                }
                else
                {
                    _pq->release(chunk);
                }
            }
            else
            {
                _pq->release(chunk);
            }
        }
        else
        {
            m.receive(_drainbuff,UDP_CHUNK+sizeof(BufHdr),MC_PORT,MC_ADDR);
            pc(0,6,"Queue is full... ");
        }
        FD_CLR(m.socket(), &rd);
    }
    return true;
}


void CliThread::_netSend(tcp_cli_sock& tcp, int& onceinawhile)
{

    BufHdr         req;

    req.frmsig = UNIQUE_HDR;
    req.saddr  = inet_addr(sock::GetLocalIP("*"));
    req.tickc  = GetTickCount();
    req.frmmax = PCTX->seq();
    req.pingt  = this->_pingt;
    req.sig    = ALIVE_DATA;
    req.seq    = PCTX->seq();
    if(++onceinawhile > CHUNKS)
    {
        pc(0,2,"Send sync sequece: %04d           ", int(req.seq));
        req.sig = SYNC_DATA;
        onceinawhile = 0;
    }
    else if(_pq->percent()<_pq->optim())
    {
        req.sig  = PLAY_DATA;
        pc(0,8,"asking for frame now");

    }
    int tmpv = tcp.send((uint8_t*)&req, sizeof(BufHdr));
    if(tmpv<=0)
    {
        pc(0,2,"Server not found. Disconnected    ");
        tcp.destroy();
    }
}

/**
 * @brief CliThread::thread_main
 * makes sure the receiving buffer is > 80% full
 */
void CliThread::thread_main()
{
    tcp_cli_sock   tcp;
    udp_group_sock m;

    int            onceinawhile = 1;
    fd_set         rd;
    uint32_t       ttt;

    _frmtime = (1000*UDP_CHUNK) / (SAMPLE_AO * CHANNELS_AO * (BITS_AO/8));
    while(__alive && !this->is_stopped())
    {
        ttt = GetTickCount();
        if(!_reconnect(tcp, m))
            continue;

        if(!_netReceive(tcp, m, SELECT_TIME))
            continue;

        _netSend(tcp, onceinawhile);

        ttt = GetTickCount()-ttt;
        if(ttt < (_frmtime))
        {
            MSLEEP(_frmtime - ttt + 1);
        }
        ::usleep(256);
    }
}

bool CliThread::_connecting(udp_group_sock& m,
                            tcp_cli_sock& tcp)
{
    int tmpv;

    _frmmax=0;
    _diff=0;
    m.drop();
    m.destroy();
    MSLEEP(256);
    pc(0,2,"Joining UDP Group %s %d", MC_ADDR, rand());
    if(m.join(MC_ADDR, MC_PORT)!=NOERROR)
    {
        perror(" Join...");
        pc(0,2,"Error Join Multicast Group               ");
        return false;
    }
    pc(0,2,"Waiting group broadcast              %d ",rand());
    if(!m.is_set(256000) && __alive)
    {
        sleep(1);
        return false;
    }

    Qbuff::Chunk*  chunk =  _pq->obtain();
    if(chunk && __alive)
    {
        pc(0,2,"                                  ");
        tmpv = m.receive(chunk->_buff,_pq->payload(),MC_PORT,MC_ADDR);
        if(tmpv !=_pq->payload())
        {
            _pq->release(chunk);
            pc(0,2,"Received invalid frame");
            return false;
        }
    }
    else
    {
        return false;
    }

    pc(0,2,"Connecting to: %s:%d", (const char*)Ip2str(chunk->_hdr->saddr),SRV_PORT+1);
    if(!tcp.is_really_connected() &&
       !tcp.isconnecting())
    {
        tcp.raw_connect(chunk->_hdr->saddr, SRV_PORT+1);
        MSLEEP(256);
        if(!tcp.is_really_connected() || !__alive)
        {
            _pq->release(chunk);
            tcp.destroy();
            pc(0,2,"Connection failed to: %s:%d", (const char*)Ip2str(chunk->_hdr->saddr),SRV_PORT+1);
            return false;
        }
        pc(0,2,"Connected to: %s:%d             ", (const char*)Ip2str(chunk->_hdr->saddr),SRV_PORT+1);
    }
    tcp.set_blocking(1);
    _pq->release(chunk);
    return __alive;
}

// command for player
void CliThread::_apply(BufHdr* ph)
{
    if(ph->frmsig == UNIQUE_HDR)
    {
        //std::cout << "tcp from srv " << Ip2str(ph->saddr) << "," << ph->seq <<"\n";
        ::memcpy(&_hdr, ph, sizeof(BufHdr));
        if(RESET_ALL == ph->sig)
        {
            _pingt    = 0;
            _cliticku = 0;
            _diff     = 0;
            _frmmax   = 0;
            _pq->reset();
            _pq->setOptim(OPTIM_QUEUE_LEN);
            _pq->setFull(ALMOST_FULL);
            pc(0,3,"All buffers were reset.");
            return;
        }

        if(SYNC_DATA == ph->sig)
        {
            uint32_t ct   = GetTickCount();
            this->_pingt  = ct - ph->tickc;
            pc(0,5,"Updating This ping: %d                                 ", _pingt);
            return;
        }
        if(ph->sig == VOLUM_SIG)
        {
            char cmd[32];
            ::sprintf(cmd,"amixer set Master %d%%", ph->bytes);
            system(cmd);
            return;
        }
        if(ph->sig == SYNC_ALL)
        {
            pc(0,3, "Received Thisping: %d, Maxping: %d Maxfrm: %d Frmtime: %d     ",
                _pingt, ph->pingmax, ph->frmmax, _frmtime);
            if(ph->pingmax==_pingt && _pingt>0)
            {
                 _pq->setOptim(OPTIM_QUEUE_LEN);
                 return;
            }

            int discard = ph->frmmax - PCTX->seq();
            int difping = ph->pingmax - _pingt;
            if(difping >_frmtime){
                discard += difping /_frmtime;
            }
            pc(0,6,"Received Max Seq %d versus This seq %d DifPing: %d",
               ph->frmmax, PCTX->seq(), discard, difping);
            if(discard > 2 && discard < (_pq->optim()-(CHUNKS/8))) //frm > 120ms
            {
                _pq->removeback(discard);
                _pq->setOptim(_pq->length());
                pc(0,7,"Rolling Que with: %d. Q Optim is:  %d", discard, _pq->optim());
            }
        }
    }
    else
    {

    }
}

