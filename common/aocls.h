#ifndef AOCLS_H
#define AOCLS_H

#include <ao/ao.h>

class AoCls
{
public:
    AoCls(int bits, int rate, int channels);
    ~AoCls();
    bool play(uint8_t* buff, size_t sz);

private:

    ao_device*       _dev;
    ao_sample_format _format;
};

#endif // AOCLS_H
