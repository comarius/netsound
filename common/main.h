#ifndef _MAIN_H_
#define _MAIN_H_
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>    /* For O_RDWR */
#include <unistd.h>   /* For open(), creat() */
#include <iostream>   /* For open(), creat() */
#include <signal.h>   /* For open(), creat() */
#include <mutex>
#include "os.h"
#include "sock.h"
#include "aoutil.h"
#include "alsacls.h"
#include "clithread.h"
#include "aocls.h"
#include "mp3cls.h"


//#define SIMUL
#define _VERSION     "0.0.1"

#define ALMOST_EMPTY 10
#define BITS         8
#define PIPE_IN      "/tmp/speakers"

#define BITS_AO     16
#define SAMPLE_AO   48000
#define CHANNELS_AO 2

extern bool __alive;

class RunCtx
{
public:
    RunCtx(RunCtx** p);
    ~RunCtx();

    void client();
    void server();
    int  seq()const{return _seq;}
private:
    int _seq=0;

};

#define MSLEEP(k)   ::usleep(k*1000)

extern RunCtx* PCTX;
extern bool __alive;

inline void pc(int x, int y, const char *format, ...)
{
    va_list args;
    va_start(args, format);
    printf("\033[%d;%dH", y, x);
    vprintf(format, args);
    va_end(args);
    fflush(stdout);
}


#endif //_MAIN_H_
