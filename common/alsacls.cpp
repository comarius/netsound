#include "alsacls.h"

Alsacls::Alsacls()
{

}

Alsacls::~Alsacls()
{
    close();
}

void Alsacls::close()
{
    snd_pcm_drain(_pcm_handle);
    snd_pcm_close(_pcm_handle);
    //delete[] _buff;
}

int Alsacls::open(int rate, int channels, int seconds)
{
    _rate =(rate);
    _channels=(channels);
    _seconds=(seconds);

    if((_pcm = snd_pcm_open(&_pcm_handle, PCM_DEVICE,
                        SND_PCM_STREAM_PLAYBACK, 0)) < 0)
    {
        printf("ERROR: Can't open \"%s\" PCM device. %s\n",
                    PCM_DEVICE, snd_strerror(_pcm));
        return 0;
    }
    snd_pcm_hw_params_alloca(&_params);
    snd_pcm_hw_params_any(_pcm_handle, _params);

    /* Set parameters */
    if ((_pcm = snd_pcm_hw_params_set_access(_pcm_handle, _params,
                    SND_PCM_ACCESS_RW_INTERLEAVED)) < 0)
        printf("ERROR: Can't set interleaved mode. %s\n", snd_strerror(_pcm));

    if ((_pcm = snd_pcm_hw_params_set_format(_pcm_handle, _params,
                        SND_PCM_FORMAT_S16_LE)) < 0)
    printf("ERROR: Can't set format. %s\n", snd_strerror(_pcm));

    if ((_pcm = snd_pcm_hw_params_set_channels(_pcm_handle, _params, _channels)) < 0)
        printf("ERROR: Can't set channels number. %s\n", snd_strerror(_pcm));

    if ((_pcm = snd_pcm_hw_params_set_rate_near(_pcm_handle, _params, &_rate, 0)) < 0)
        printf("ERROR: Can't set rate. %s\n", snd_strerror(_pcm));

    /* Write parameters */
    if ((_pcm = snd_pcm_hw_params(_pcm_handle, _params)) < 0)
        printf("ERROR: Can't set harware parameters. %s\n", snd_strerror(_pcm));

    /* Resume information */
    printf("PCM name: '%s'\n", snd_pcm_name(_pcm_handle));

    printf("PCM state: %s\n", snd_pcm_state_name(snd_pcm_state(_pcm_handle)));
    unsigned int tmp;
    snd_pcm_hw_params_get_channels(_params, &tmp);
    printf("channels: %i ", tmp);

    if (tmp == 1)
        printf("(mono)\n");
    else if (tmp == 2)
        printf("(stereo)\n");

    snd_pcm_hw_params_get_rate(_params, &tmp, 0);
    printf("rate: %d bps\n", tmp);

    printf("seconds: %d\n", _seconds);

    /* Allocate buffer to hold single period */
    snd_pcm_hw_params_get_period_size(_params, &_frames, 0);

    int buff_size = _frames * _channels * 2 /* 2 -> sample size */;
 //   _buff = new uint8_t[buff_size];

    snd_pcm_hw_params_get_period_time(_params, &tmp, NULL);

    return buff_size;
}

void Alsacls::playbuffer(uint8_t * pb, int len)
{
    (void)len;
    if ((_pcm = snd_pcm_writei(_pcm_handle, pb, _frames)) == -EPIPE)
    {
        snd_pcm_prepare(_pcm_handle);
    }
    else if (_pcm < 0)
    {
        printf("ERROR. Can't write to PCM device. %s\n", snd_strerror(_pcm));
    }


}
