#!/bin/bash
rm -r build-*
pushd serversrc/
    [[ -f Makefile ]] && make clean
    rm -r CMakeFiles/
    rm CMakeCache.txt
    rm Makefile cmake_install.cmake *.pro.user
popd
pushd clientsrc/
    [[ -f Makefile ]] && make clean
    rm -r CMakeFiles/
    rm CMakeCache.txt
    rm Makefile cmake_install.cmake *.pro.user
popd

