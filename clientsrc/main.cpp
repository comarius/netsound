
#include <set>
#include "main.h"
#include "cpipein.h"
#include "clithread.h"
#include "aoutil.h"
#include "mp3cls.h"
#include "alsacls.h"
#include "aocls.h"
#include "clictx.h"

bool    __alive = true;
RunCtx* PCTX;

/*
 *             chk->_hdr->a_bits=16;
            chk->_hdr->a_byte=4;
            chk->_hdr->a_channels=2;
            chk->_hdr->a_rate=48000;

 */


void block_signal(int signal_to_block /* i.e. SIGPIPE */ )
{
    sigset_t set;
    sigemptyset(&set);
    sigaddset(&set, signal_to_block);
    sigset_t old_state;
    sigprocmask(SIG_BLOCK, &set, &old_state);
}

void notapl(int s)
{
    (void)s;
}

void ctrlc(int s)
{
    (void)s;
    __alive=false;
}


int main(int argc, char *argv[])
{
    (void)argc;
    (void)argv;
    RunCtx  ctx(&PCTX);
    ctx.client();
}

RunCtx::RunCtx(RunCtx** p)
{
    *p = this;
    block_signal(SIGPIPE);
    signal(SIGINT, ctrlc);
    //signal(SIGPIPE, notapl);
}

RunCtx::~RunCtx()
{

}


void RunCtx::client()
{
    Qbuff         qb(CHUNKS, UDP_CHUNK);
    Qbuff::Chunk* pk;
    CliThread     t(&qb);
    Alsacls       alsa;
    AoCls         ao(BITS_AO, SAMPLE_AO, CHANNELS_AO);
    //int           buffsz = alsa.open(SAMPLE_AO, CHANNELS_AO, 0);
    uint32_t      ct = GetTickCount();
    uint32_t      ttt = ct;
    size_t        bytesec = 0;
    bool          filling = true;
    size_t        sleept = (1000*UDP_CHUNK) / (SAMPLE_AO * CHANNELS_AO * (BITS_AO/8));
    int           fillingi=0;
    char          qqq[65] = {0};

    system("clear");
    pc(0,0, "spkclient ver: %s",_VERSION);

    t.start_thread();
    while(__alive)
    {
        ttt = GetTickCount();
        if(filling == true && qb.percent()<qb.optim())
        {
            pc(0,8,"filling queue     %d to optim = %d", fillingi++,qb.optim());
            MSLEEP(sleept);
            for(int i=0;i<qb.maxsz();i++)
            {
                if(i<qb.length())
                    qqq[i]='*';
                else
                    qqq[i]='.';
            }
            pc(0,7,"%s",qqq);
            continue;
        }
        filling=false;
        pk =  qb.getfront();
        if(pk != nullptr)
        {
            if(pk->_hdr->bytes)
            {
                _seq = pk->_hdr->seq;
                if(qb.percent()>qb.almostFull())
                {
                    pc(0,11,"Error skipping frame     %d", fillingi++);
                    continue;
                }

                ao.play(pk->payload(),pk->_hdr->bytes);
                bytesec += pk->_hdr->bytes;
                pk->_hdr->bytes=0;
                size_t ctt = GetTickCount();
                if(ctt-ct>=sleept)
                {
                    pc(0,1 ,"Q Size: %d Q Percent: %d%% Frame: %d Speed: %dKb/s", qb.maxsz(),
                                                                            qb.percent(),
                                                                            pk->_hdr->seq,
                                                                            (bytesec * (1000/sleept)/1000));
                    ct = ctt;
                    bytesec = 0;
                    for(int i=0;i<qb.maxsz();i++)
                    {
                        if(i<qb.length())
                            qqq[i]='*';
                        else
                            qqq[i]='.';
                    }
                    pc(0,7,"%s",qqq);
                }
            }
            pk->_hdr->bytes=0;
            qb.release(pk);
        }
        if(qb.percent()<qb.optim()/2)
        {
            MSLEEP(16);
        }
        if(qb.percent()<2)
        {
            filling=true;
        }
        MSLEEP(sleept);
    }
    t.stop_thread();
}
