#QT += core
QT -= gui

TARGET = spkclient
CONFIG += console
CONFIG -= app_bundle
QMAKE_CXXFLAGS +=  -std=c++11 -Wno-unused-paramater
TEMPLATE = app
INCLUDEPATH = ./../common ./

SOURCES += main.cpp \
    ../common/alsacls.cpp \
    ../common/aocls.cpp \
    ../common/aoutil.cpp \
    ../common/clictx.cpp \
    ../common/clithread.cpp \
    ../common/cpipein.cpp \
    ../common/mp3cls.cpp \
    ../common/sock.cpp
HEADERS += \
    ../common/alsacls.h \
    ../common/aocls.h \
    ../common/aoutil.h \
    ../common/clictx.h \
    ../common/clithread.h \
    ../common/cpipein.h \
    ../common/main.h \
    ../common/mp3cls.h \
    ../common/os.h \
    ../common/sock.h
#sudo apt-get install libmpg123-dev libao-dev





LIBS +=  -lasound -lmpg123 -lao
