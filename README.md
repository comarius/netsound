# Multi room sound system (C++, simple, fast, minimal)
### Low Latency multi room speaker system.

## block diagram

![alt text](https://raw.githubusercontent.com/comarius/speakers/master/docs/block.png "approximative block diagram")


## sequence diagram

![alt text](https://raw.githubusercontent.com/comarius/speakers/master/docs/sequence.png "sequence dia")



## Howto
    - Have a linux OS
    - install prerequisites

```javascript
sudo apt-get install libao-dev
sudo apt-get install libalsa-dev
sudo apt-get install libmpg123-dev
sudo route add -net 240.0.0.0 netmask 240.0.0.0 <IFACE>
```

    - clone the git
      - git clone https://github.com/comarius/speakers
      - cd in srv and cli , cmake. and make
        - cd /speakers/serversrc (then /clientsrc)
          - qmake .
          - make
    - copy the client binaries on a separate machine(s)
    - start server:
      - cd ./serversrc and run it
        - ./spkserver
      - goto ./test folder
        - run one of the bash sh files.
      - on same computer or on a separate machine run the client
        - make sure the machines are on the same network.
      
      - http query access. like 
        - http://serverip/speakers           returns coma separates speaker id's
        - http://serverip/speaker/s/v        s-speaker v-volume % 
                      
    
    
    
    
## Free for non profit. Specific license / speaker(s).
